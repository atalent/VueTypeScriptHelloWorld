import Vue from 'vue';
import App from './App.vue';

Vue.config.productionTip = true;

new Vue({
    render: h => h(App)
}).$mount('#app');


function greeter(person: string) {
    return "Hello, " + person;
}

let user = "Ts";

var aa = document.getElementById("app3");
if (aa != null)
    aa.innerHTML = greeter(user);